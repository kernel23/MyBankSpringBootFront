import React, { useState } from "react";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";

import Home from "./components/Home";
import Login from "./components/Login";

import "./App.css";
import money from "./money.jpg";

export const AuthContext = React.createContext();

function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        rest.isAuthenticated ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
}

function App() {
  const wrapper = React.createRef();
  const [token, setToken] = useState(
    document.cookie.split(";")[0] && document.cookie.split(";")[0].split("=")[1]
  );
  const [role, setRole] = useState(
    document.cookie.split(";")[1] && document.cookie.split(";")[1].split("=")[1]
  );
  const [userName, setUsername] = useState(
    document.cookie.split(";")[2] && document.cookie.split(";")[2].split("=")[1]
  );
  const tokenFromCookie =
    document.cookie.split(";")[0] &&
    document.cookie.split(";")[0].split("=")[1];

  const [isAuthenticated, setIsAuthenticated] = useState(!!tokenFromCookie);

  return (
    <AuthContext.Provider
      value={{
        token,
        role,
        userName,
        setToken,
        setRole,
        setUsername,
        setIsAuthenticated,
      }}
    >
      <Router>
        <div
          className="App"
          style={{
            backgroundImage: "url(" + money + ")",
            margin: 0,
            padding: 0,
            backgroundSize: "cover",
            height: "100vh",
            width: "100vw",
          }}
          ref={wrapper}
        >
          <Switch>
            <Route exact path="/">
              {!isAuthenticated ? <Redirect to="/login" /> : <Home />}
            </Route>
            <PrivateRoute path="/home" isAuthenticated={isAuthenticated}>
              <Home />
            </PrivateRoute>
            <Route path="/login">
              <Login />
            </Route>
          </Switch>
        </div>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
