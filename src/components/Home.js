import React, { useReducer, useContext, useEffect, useState } from "react";
import axios from "axios";

import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";
import DeleteIcon from "@material-ui/icons/Delete";
import CheckIcon from "@material-ui/icons/Check";
import ClearIcon from "@material-ui/icons/Clear";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";

import { AuthContext } from "../App";

import "./Home.css";

const initialState = {
  inputValue: "",
  virValue: "",
  viirValue: "",
  virToValue: "",
  retValue: "",
  forexValue: "",
  accountInformations: {
    isExpanded: true,
    informations: {},
  },
  accountOperations: {
    isExpanded: true,
    operations: [],
  },
  operationsList: {
    isExpanded: true,
  },
  forexInfos: {},
};

const getAccountInformations = async (accountId, token) => {
  try {
    const response = await axios.get(
      `http://ec2-13-52-254-156.us-west-1.compute.amazonaws.com:8081/api/bank/comptes/${accountId}`,
      { headers: { Authorization: token } }
    );
    return response.data;
  } catch (error) {
    console.error(error);
  }
};

const getAccountOperations = async (accountId, token) => {
  try {
    const response = await axios.get(
      `http://ec2-13-52-254-156.us-west-1.compute.amazonaws.com:8081/api/bank/operations/comptes/${accountId}`,
      { headers: { Authorization: token } }
    );
    return response.data;
  } catch (error) {
    console.error(error);
  }
};

const makeTransfer = async (accountId, ammount, token) => {
  try {
    await axios.post(
      `http://ec2-13-52-254-156.us-west-1.compute.amazonaws.com:8081/api/bank/comptes/${accountId}/versOperation/`,
      {
        montant: ammount,
      },
      { headers: { Authorization: token } }
    );
  } catch (error) {
    console.error(error);
  }
};

const makeTransfer2 = async (accountId, ammount, receptionist, token) => {
  try {
    await axios.post(
      `http://ec2-13-52-254-156.us-west-1.compute.amazonaws.com:8081/api/bank/comptes/${accountId}/versementOperation/`,
      {
        montant: ammount,
        codeCompte: receptionist,
      },
      { headers: { Authorization: token } }
    );
  } catch (error) {
    console.error(error);
  }
};

const makeWidraw = async (accountId, ammount, token) => {
  try {
    await axios.post(
      `http://ec2-13-52-254-156.us-west-1.compute.amazonaws.com:8081/api/bank/comptes/${accountId}/retOperation/`,
      {
        montant: ammount,
      },
      { headers: { Authorization: token } }
    );
  } catch (error) {
    console.error(error);
  }
};

const getForex = async (forex, token) => {
  try {
    const response = await axios.get(
      `http://ec2-13-52-254-156.us-west-1.compute.amazonaws.com:8081/api/bank/forex/${forex}`,
      { headers: { Authorization: token } }
    );
    return response.data.rates || {};
  } catch (error) {
    console.error(error);
  }
};

const getAccountOperationsForSpecificUser = async (
  userName,
  token,
  dispatch,
  setComptes
) => {
  try {
    const response = await axios.get(
      `http://ec2-13-52-254-156.us-west-1.compute.amazonaws.com:8081/api/bank/operations?username=${userName}`,
      { headers: { Authorization: token } }
    );
    const comptesIds =
      response.data &&
      response.data.map((element) => element.compte.codeCompte);
    const uniqComptesIds = [...new Set(comptesIds)];

    let comptes = [];
    uniqComptesIds &&
      uniqComptesIds.forEach((id) => {
        const foundElement =
          response.data &&
          response.data.find((element) => id === element.compte.codeCompte);
        foundElement && comptes.push(foundElement.compte);
      });

    setComptes(comptes);
    dispatch({
      type: "expandOperationsInformations",
      payload: response.data,
    });
  } catch (error) {
    console.error(error);
  }
};

function reducer(state, action) {
  switch (action.type) {
    case "changeInput":
      return { ...state, inputValue: action.payload };
    case "changeVirValue":
      return { ...state, virValue: action.payload };
    case "changeViirValue":
      return { ...state, viirValue: action.payload };
    case "changeVirToValue":
      return { ...state, virToValue: action.payload };
    case "changeRetValue":
      return { ...state, retValue: action.payload };
    case "changeForex":
      return { ...state, forexValue: action.payload };
    case "expandForex":
      return { ...state, forexInfos: action.payload };
    case "expandAccountInformations":
      return {
        ...state,
        accountInformations: action.payload
          ? { isExpanded: true, informations: action.payload }
          : {
              isExpanded: true,
              informations: state.accountInformations.informations,
            },
      };
    case "hideAccountInformations":
      return {
        ...state,
        accountInformations: {
          isExpanded: false,
          informations: state.accountInformations.informations,
        },
      };
    case "emptyInformations":
      return {
        ...state,
        accountInformations: { isExpanded: false, informations: {} },
        accountOperations: { isExpanded: false, operations: [] },
        operationsList: { isExpanded: false, operations: [] },
        inputValue: "",
      };
    case "expandOperationsInformations":
      return {
        ...state,
        accountOperations:
          action.payload && typeof action.payload === "object"
            ? { isExpanded: true, operations: action.payload }
            : {
                isExpanded: true,
                operations: state.accountOperations.operations,
              },
      };
    case "hideOperationsInformations":
      return {
        ...state,
        accountOperations: {
          isExpanded: false,
          operations: state.accountOperations.operations,
        },
      };
    case "expandOperationsList":
      return { ...state, operationsList: { isExpanded: true } };
    case "hideOperationsList":
      return { ...state, operationsList: { isExpanded: false } };
    default:
      throw new Error();
  }
}

export default function Home() {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [comptes, setComptes] = useState([]);
  const { token, role, userName, setToken, setIsAuthenticated } = useContext(
    AuthContext
  );
  const AuthStr = "Bearer ".concat(token);

  useEffect(() => {
    if (role !== "ADMIN") {
      getAccountOperationsForSpecificUser(
        userName,
        AuthStr,
        dispatch,
        setComptes
      );
    }
  }, [userName, AuthStr, role]);

  const logout = () => {
    document.cookie = "token=";
    document.cookie = "role=";
    document.cookie = "username=";
    setToken("");
    setIsAuthenticated(false);
  };

  return (
    <>
      <Grid container className="panels-container">
        <Grid item xs={12}>
          <Typography variant="h2" gutterBottom>
            My Bank
          </Typography>
        </Grid>
        {role === "ADMIN" && (
          <>
            <Grid container spacing={10}>
              <Grid item xs={6}>
                <ExpansionPanel expanded className="expansion-panel">
                  <ExpansionPanelSummary
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                    className="expansion-panel-summary"
                  >
                    <Typography>Consultation d'un compte</Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails className="expansion-panel-details">
                    <Paper className="paper-input">
                      <InputBase
                        placeholder="Entrez le nom de l'utilisateur"
                        value={state.inputValue}
                        type="text"
                        inputProps={{ "aria-label": "search google maps" }}
                        onKeyDown={async (event) => {
                          if (event.key === "Enter") {
                            const informations = await getAccountInformations(
                              state.inputValue,
                              AuthStr
                            );
                            const operations = await getAccountOperations(
                              state.inputValue,
                              AuthStr
                            );
                            dispatch({
                              type: "expandAccountInformations",
                              payload: informations,
                            });
                            dispatch({
                              type: "expandOperationsInformations",
                              payload: operations,
                            });
                            dispatch({ type: "expandOperationsList" });
                          }
                        }}
                        onChange={(event) => {
                          dispatch({
                            type: "changeInput",
                            payload: event.target.value,
                          });
                        }}
                      />
                      <IconButton
                        aria-label="search"
                        onClick={async () => {
                          const informations = await getAccountInformations(
                            state.inputValue,
                            AuthStr
                          );
                          const operations = await getAccountOperations(
                            state.inputValue,
                            AuthStr
                          );
                          dispatch({
                            type: "expandAccountInformations",
                            payload: informations,
                          });
                          dispatch({
                            type: "expandOperationsInformations",
                            payload: operations && operations.content,
                          });
                          dispatch({ type: "expandOperationsList" });
                        }}
                      >
                        <SearchIcon />
                      </IconButton>
                    </Paper>
                    <IconButton
                      aria-label="delete"
                      onClick={() => {
                        dispatch({ type: "emptyInformations" });
                      }}
                    >
                      <DeleteIcon />
                    </IconButton>
                    <Paper className="paper-input">
                      <InputBase
                        placeholder="Entrez le forex"
                        value={state.forexValue}
                        type="text"
                        onKeyDown={async (event) => {
                          if (event.key === "Enter") {
                            const forex = await getForex(
                              state.forexValue,
                              AuthStr
                            );
                            dispatch({
                              type: "expandForex",
                              payload: forex,
                            });
                          }
                        }}
                        onChange={(event) => {
                          dispatch({
                            type: "changeForex",
                            payload: event.target.value,
                          });
                        }}
                      />
                      <IconButton
                        aria-label="search"
                        onClick={async () => {
                          const forex = await getForex(
                            state.forexValue,
                            AuthStr
                          );
                          dispatch({
                            type: "expandForex",
                            payload: forex,
                          });
                        }}
                      >
                        <SearchIcon />
                      </IconButton>
                    </Paper>
                    <IconButton
                      aria-label="delete"
                      onClick={() => {
                        dispatch({ type: "changeForex", payload: "" });
                        dispatch({ type: "expandForex", payload: {} });
                      }}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </Grid>

              {state.accountInformations.informations &&
                state.accountInformations.informations.codeCompte && (
                  <Grid item xs={6}>
                    <ExpansionPanel expanded className="expansion-panel">
                      <ExpansionPanelSummary
                        aria-controls="panel1a-content"
                        id="panel1a-header"
                        className="expansion-panel-summary"
                      >
                        <Typography>
                          Faire des opérations sur le compte
                        </Typography>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails className="expansion-panel-details">
                        <Paper className="paper-input">
                          <div style={{ display: "flex" }}>
                            <InputBase
                              placeholder="Faire un virement à un compte"
                              value={state.viirValue}
                              type="number"
                              onChange={(event) => {
                                dispatch({
                                  type: "changeViirValue",
                                  payload: event.target.value,
                                });
                              }}
                            />
                            <hr />
                            <InputBase
                              placeholder="Compte de réception"
                              value={state.virToValue}
                              onChange={(event) => {
                                dispatch({
                                  type: "changeVirToValue",
                                  payload: event.target.value,
                                });
                              }}
                            />
                            <IconButton
                              onClick={async () => {
                                await makeTransfer2(
                                  state.accountInformations.informations
                                    .codeCompte,
                                  state.viirValue,
                                  state.virToValue,
                                  AuthStr
                                );
                                const operations = await getAccountOperations(
                                  state.accountInformations.informations
                                    .codeCompte,
                                  AuthStr
                                );
                                const informations = await getAccountInformations(
                                  state.inputValue,
                                  AuthStr
                                );
                                dispatch({
                                  type: "expandAccountInformations",
                                  payload: informations,
                                });
                                dispatch({
                                  type: "expandOperationsInformations",
                                  payload: operations,
                                });
                                dispatch({
                                  type: "changeViirValue",
                                  payload: "",
                                });
                                dispatch({
                                  type: "changeVirToValue",
                                  payload: "",
                                });
                              }}
                            >
                              <CheckIcon />
                            </IconButton>
                            <IconButton
                              aria-label="delete"
                              onClick={() => {
                                dispatch({
                                  type: "changeViirValue",
                                  payload: "",
                                });
                                dispatch({
                                  type: "changeVirToValue",
                                  payload: "",
                                });
                              }}
                            >
                              <ClearIcon />
                            </IconButton>
                          </div>
                          <div>
                            <InputBase
                              placeholder="Faire un virement"
                              value={state.virValue}
                              type="number"
                              onChange={(event) => {
                                dispatch({
                                  type: "changeVirValue",
                                  payload: event.target.value,
                                });
                              }}
                            />
                            <IconButton
                              onClick={async () => {
                                await makeTransfer(
                                  state.accountInformations.informations
                                    .codeCompte,
                                  state.virValue,
                                  AuthStr
                                );
                                const operations = await getAccountOperations(
                                  state.accountInformations.informations
                                    .codeCompte,
                                  AuthStr
                                );
                                const informations = await getAccountInformations(
                                  state.inputValue,
                                  AuthStr
                                );
                                dispatch({
                                  type: "expandAccountInformations",
                                  payload: informations,
                                });
                                dispatch({
                                  type: "expandOperationsInformations",
                                  payload: operations,
                                });
                                dispatch({
                                  type: "changeVirValue",
                                  payload: "",
                                });
                              }}
                            >
                              <CheckIcon />
                            </IconButton>
                            <IconButton
                              aria-label="delete"
                              onClick={() => {
                                dispatch({
                                  type: "changeVirValue",
                                  payload: "",
                                });
                              }}
                            >
                              <ClearIcon />
                            </IconButton>
                          </div>

                          <div>
                            <InputBase
                              placeholder="Faire un retait"
                              value={state.retValue}
                              type="number"
                              onChange={(event) => {
                                dispatch({
                                  type: "changeRetValue",
                                  payload: event.target.value,
                                });
                              }}
                            />
                            <IconButton
                              onClick={async () => {
                                await makeWidraw(
                                  state.accountInformations.informations
                                    .codeCompte,
                                  state.retValue,
                                  AuthStr
                                );
                                const operations = await getAccountOperations(
                                  state.accountInformations.informations
                                    .codeCompte,
                                  AuthStr
                                );
                                const informations = await getAccountInformations(
                                  state.inputValue,
                                  AuthStr
                                );
                                dispatch({
                                  type: "expandAccountInformations",
                                  payload: informations,
                                });
                                dispatch({
                                  type: "expandOperationsInformations",
                                  payload: operations,
                                });
                                dispatch({
                                  type: "changeRetValue",
                                  payload: "",
                                });
                              }}
                            >
                              <CheckIcon />
                            </IconButton>
                            <IconButton
                              aria-label="delete"
                              onClick={() => {
                                dispatch({
                                  type: "changeRetValue",
                                  payload: "",
                                });
                              }}
                            >
                              <ClearIcon />
                            </IconButton>
                          </div>
                        </Paper>
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  </Grid>
                )}
              {state.forexInfos && Object.keys(state.forexInfos)[0] && (
                <Grid item xs={6}>
                  <ExpansionPanel expanded className="expansion-panel">
                    <ExpansionPanelSummary
                      aria-controls="panel1a-content"
                      id="panel1a-header"
                      className="expansion-panel-summary"
                    >
                      <Typography>Forex</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails className="expansion-panel-details">
                      <span>Forex : {Object.keys(state.forexInfos)[0]}</span>
                      <hr />
                      <span>
                        Taux:{" "}
                        {
                          state.forexInfos[
                            `${Object.keys(state.forexInfos)[0]}`
                          ].rate
                        }
                      </span>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                </Grid>
              )}
            </Grid>
            <Grid container spacing={10}>
              <Grid item xs={6}>
                <ExpansionPanel
                  expanded={state.accountOperations.isExpanded}
                  className="expansion-panel"
                  onChange={(_event, expanded) => {
                    dispatch({
                      type: expanded
                        ? "expandOperationsInformations"
                        : "hideOperationsInformations",
                    });
                  }}
                >
                  <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    className="expansion-panel-summary"
                    id="panel1a-header"
                  >
                    <Typography>Dérnières opérations sur le compte</Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails className="expansion-panel-details">
                    <TableContainer className="table-container">
                      <Table aria-label="simple table">
                        <TableHead>
                          <TableRow>
                            <TableCell>Numéro de l'opération</TableCell>
                            {role !== "ADMIN" && (
                              <TableCell>Compte associé</TableCell>
                            )}
                            <TableCell align="right">
                              Date de l'opération
                            </TableCell>
                            <TableCell align="right">Montant</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {state.accountOperations.operations &&
                            state.accountOperations.operations.map((row) => (
                              <TableRow key={row.numero}>
                                <TableCell component="th" scope="row">
                                  {row.numero}
                                </TableCell>
                                {role === "USER" && (
                                  <TableCell component="th" scope="row">
                                    {row.compte.codeCompte}
                                  </TableCell>
                                )}
                                <TableCell align="right">
                                  {new Date(
                                    row.dateCreation
                                  ).toLocaleDateString("fr-FR", {
                                    weekday: "long",
                                    year: "numeric",
                                    month: "long",
                                    day: "numeric",
                                  })}
                                </TableCell>
                                <TableCell align="right">
                                  {row.montant}
                                </TableCell>
                              </TableRow>
                            ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </Grid>
              <Grid item xs={6}>
                <ExpansionPanel
                  className="expansion-panel"
                  expanded={state.accountInformations.isExpanded}
                  onChange={(event, expanded) => {
                    dispatch({
                      type: expanded
                        ? "expandAccountInformations"
                        : "hideAccountInformations",
                    });
                  }}
                >
                  <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    className="expansion-panel-summary"
                    id="panel1a-header"
                  >
                    <Typography>Informations sur le compte</Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails className="expansion-panel-details1">
                    <p>
                      Code:{" "}
                      {state.accountInformations.informations &&
                        state.accountInformations.informations.codeCompte}{" "}
                    </p>
                    <p>
                      Date creation:{" "}
                      {state.accountInformations.informations &&
                        new Date(
                          state.accountInformations.informations.dateCreation
                        ).toLocaleDateString("fr-FR", {
                          weekday: "long",
                          year: "numeric",
                          month: "long",
                          day: "numeric",
                        })}
                    </p>
                    {state.accountInformations.informations.solde && (
                      <p>
                        Solde : {state.accountInformations.informations.solde}
                      </p>
                    )}
                    {state.accountInformations.informations.decouvert && (
                      <p>
                        Decouvert :{" "}
                        {state.accountInformations.informations.decouvert}
                      </p>
                    )}
                    {state.accountInformations.informations.taux && (
                      <p>
                        Taux : {state.accountInformations.informations.taux}
                      </p>
                    )}
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </Grid>
            </Grid>
          </>
        )}
        {role === "USER" && (
          <>
            <Grid container spacing={10} justify="center">
              <Grid item>
                <ExpansionPanel
                  expanded={state.accountOperations.isExpanded}
                  className="expansion-panel"
                  onChange={(_event, expanded) => {
                    dispatch({
                      type: expanded
                        ? "expandOperationsInformations"
                        : "hideOperationsInformations",
                    });
                  }}
                >
                  <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    className="expansion-panel-summary"
                    id="panel1a-header"
                  >
                    <Typography>Dérnières opérations sur le compte</Typography>
                  </ExpansionPanelSummary>
                  <ExpansionPanelDetails className="expansion-panel-details">
                    <TableContainer className="table-container">
                      <Table aria-label="simple table">
                        <TableHead>
                          <TableRow>
                            <TableCell>Numéro de l'opération</TableCell>
                            {role !== "ADMIN" && (
                              <TableCell>Compte associé</TableCell>
                            )}
                            <TableCell align="right">
                              Date de l'opération
                            </TableCell>
                            <TableCell align="right">Montant</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {state.accountOperations.operations &&
                            state.accountOperations.operations.map((row) => (
                              <TableRow key={row.numero}>
                                <TableCell component="th" scope="row">
                                  {row.numero}
                                </TableCell>
                                {role === "USER" && (
                                  <TableCell component="th" scope="row">
                                    {row.compte.codeCompte}
                                  </TableCell>
                                )}
                                <TableCell align="right">
                                  {new Date(
                                    row.dateCreation
                                  ).toLocaleDateString("fr-FR", {
                                    weekday: "long",
                                    year: "numeric",
                                    month: "long",
                                    day: "numeric",
                                  })}
                                </TableCell>
                                <TableCell align="right">
                                  {row.montant}
                                </TableCell>
                              </TableRow>
                            ))}
                        </TableBody>
                      </Table>
                    </TableContainer>
                  </ExpansionPanelDetails>
                </ExpansionPanel>
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              {comptes &&
                comptes.map((compte) => (
                  <Grid item xs="auto" key={compte.codeCompte}>
                    <ExpansionPanel
                      className="expansion-panel"
                      expanded={state.accountInformations.isExpanded}
                      onChange={(event, expanded) => {
                        dispatch({
                          type: expanded
                            ? "expandAccountInformations"
                            : "hideAccountInformations",
                        });
                      }}
                    >
                      <ExpansionPanelSummary
                        expandIcon={<ExpandMoreIcon />}
                        aria-controls="panel1a-content"
                        className="expansion-panel-summary"
                        id="panel1a-header"
                      >
                        <Typography>Informations sur le compte</Typography>
                      </ExpansionPanelSummary>
                      <ExpansionPanelDetails className="expansion-panel-details1">
                        <p>Code: {compte && compte.codeCompte} </p>
                        <p>
                          Date creation:{" "}
                          {compte &&
                            new Date(compte.dateCreation).toLocaleDateString(
                              "fr-FR",
                              {
                                weekday: "long",
                                year: "numeric",
                                month: "long",
                                day: "numeric",
                              }
                            )}
                        </p>
                        {compte.solde && <p>Solde : {compte.solde}</p>}
                        {compte.decouvert && (
                          <p>Decouvert : {compte.decouvert}</p>
                        )}
                        {compte.taux && <p>Taux : {compte.taux}</p>}
                      </ExpansionPanelDetails>
                    </ExpansionPanel>
                  </Grid>
                ))}
            </Grid>
          </>
        )}
      </Grid>
      <IconButton color="secondary" className="logout" onClick={logout}>
        <ExitToAppIcon />
      </IconButton>
    </>
  );
}
