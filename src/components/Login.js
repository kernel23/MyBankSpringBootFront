import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";

import Paper from "@material-ui/core/Paper";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";

import { AuthContext } from "../App";

import "react-notifications/lib/notifications.css";
import "./Login.css";

const Login = () => {
  const [pseudo, setPseudo] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [userName, setUserName] = useState("");
  const [signUpPassword, setSignUpPassword] = useState("");
  const [tab, setTab] = useState("login");
  const { setToken, setRole, setUsername, setIsAuthenticated } = useContext(
    AuthContext
  );
  let history = useHistory();

  const authenticate = async (...args) => {
    try {
      const res = await axios.post(
        "http://ec2-13-52-254-156.us-west-1.compute.amazonaws.com:8081/api/auth/signin",
        {
          username: args[0],
          password: args[1],
        }
      );
      args[2](res.data.token);
      let role =
        res.data.authorities &&
        res.data.authorities.reduce((accu, item) => {
          switch (item.authority) {
            case "ROLE_ADMIN":
              return [...accu, "ADMIN"];
            default:
              return [...accu, "USER"];
          }
        }, []);
      role = role.includes("ADMIN") ? "ADMIN" : "USER";
      args[3](role);
      document.cookie = `token=${res.data.token}`;
      document.cookie = `role=${role}`;
      document.cookie = `username=${res.data.username}`;
      args[4](res.data.username);
      args[5](true);
      history.push("/home");
    } catch (error) {
      NotificationManager.error("Login failed", null, 3000);
    }
  };

  const signup = async (...args) => {
    try {
      await axios.post(
        "http://ec2-13-52-254-156.us-west-1.compute.amazonaws.com:8081/api/auth/signup",
        {
          name: args[0],
          email: args[1],
          username: args[2],
          password: args[3],
          role: ["user"],
        }
      );

      setName("");
      setEmail("");
      setUserName("");
      setSignUpPassword("");
      NotificationManager.success(
        "L'utilisateur a été crée avec succès, connectez-vous",
        null,
        5000
      );
      setTab("login");
    } catch (error) {
      const status = error.message.match(/(\d+)$/g)[0];
      if (status === "400") {
        NotificationManager.error("Cet utilisateur existe déjà ", null, 5000);
      } else {
        NotificationManager.error(
          "Un problème a été crée durant la création de l'utilisateur ",
          null,
          3000
        );
      }
    }
  };

  return (
    <>
      <Paper className="Paper" square>
        <Tabs
          value={tab}
          indicatorColor="primary"
          textColor="primary"
          onChange={(_event, newTab) => setTab(newTab)}
          aria-label="disabled tabs example"
        >
          <Tab value="login" label="se connecter" />
          <Tab value="signup" label="s'enregistrer" />
        </Tabs>
        {tab === "login" ? (
          <>
            <TextField
              variant="outlined"
              label="Pseudo"
              value={pseudo}
              onChange={(event) => setPseudo(event.target.value)}
            />
            <TextField
              variant="outlined"
              label="Mot de passe"
              type="password"
              value={password}
              onChange={(event) => setPassword(event.target.value)}
            />
            <div className="Buttons-container">
              <Button
                variant="contained"
                color="primary"
                onClick={authenticate.bind(
                  this,
                  pseudo,
                  password,
                  setToken,
                  setRole,
                  setUsername,
                  setIsAuthenticated
                )}
              >
                se connecter
              </Button>
            </div>
          </>
        ) : (
          <>
            <TextField
              variant="outlined"
              label="Nom et prénom"
              value={name}
              onChange={(event) => setName(event.target.value)}
            />
            <TextField
              variant="outlined"
              label="Email"
              value={email}
              onChange={(event) => setEmail(event.target.value)}
            />
            <TextField
              variant="outlined"
              label="Pseudo"
              value={userName}
              onChange={(event) => setUserName(event.target.value)}
            />
            <TextField
              variant="outlined"
              label="Mot de passe"
              type="password"
              value={signUpPassword}
              onChange={(event) => setSignUpPassword(event.target.value)}
            />
            <div className="Buttons-container">
              <Button
                variant="contained"
                color="primary"
                onClick={signup.bind(
                  this,
                  name,
                  email,
                  userName,
                  signUpPassword,
                  setToken,
                  setIsAuthenticated
                )}
              >
                s'enregistrer
              </Button>
            </div>
          </>
        )}
      </Paper>
      <NotificationContainer />
    </>
  );
};

export default Login;
